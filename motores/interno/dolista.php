<?php
/**********************************************************************************************
*	Hanumat. PHP framework for fast and secure web application development
*
*	This file is part of Hanumat.
*	Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*	Hanumat is free software: you can redistribute it and/or modify
*	it under the terms of the GNU Affero General Public License as
*	published by the Free Software Foundation, either version 3 of the
*	License, or (at your option) any later version.
*
*	Hanumat is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU Affero General Public License for more details.
*
*	You should have received a copy of the GNU Affero General Public License
*	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*	Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*	Este archivo es parte de Hanumat.
*	Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*	Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*	bajo los términos de la Licencia Pública General GNU Affero tal y como
*	ha sido publicada por la Free Software Foundation, tanto la versión 3
*	de la Licencia o cualquier otra posterior.
*
*	Hanumat es distribuido en la esperanza de que llegue a ser útil,
*	pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*	MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*	Licencia Pública General GNU Affero para más detalles.
*
*	Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*	con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
//session_start();
require_once('defs.php');
require_once('conexion.php');
require_once('funciones.php');
$retval = "";
if (isset($_POST['token'])) {
	header('Content-Type: text/html; charset=utf-8');
	if ($dbcon = conectaDB()) {
		if (validaToken($_POST['token'], $dbcon, $_POST['r'], $_POST['idU'])) {
			//Aquí empieza lo gordo...
			$tabla = esclarece($_POST['t']);
			$arrJSON = array("registros" => array(), "error" => "0");
			$llave = esclarece($_POST['l']);
			$cond = resuelveCond($_POST['c']);
			//Generar aquí el resultado en JSON...
			$arrJSON['registros'] = generaVista($tabla, $llave, $dbcon, $cond);
			$retval = json_encode($arrJSON);
		} else {
			$retval = '{"error":"14", "errmsg":"Token inválido"}';
		}
	} else {
		$retval = '{"error":"21", "errmsg":"No hay datos"}';
	}
} else {
	//Petición incorrecta
	$retval = '{"error":"11", "errmsg":"Sesión inválida"}';
}
echo $retval;

function generaVista($tabla, $campos, $dbcon, $condicion = "") {
	//Esta función debe de cambiar por una consulta ajax desde el cliente...
	$retval = array();
	$binarias = array();
	if ($condicion != "") $condicion = " where " . $condicion;
	$qry="select $campos from $tabla $condicion;";
	error_log("Consulta: $qry");
	$result=$dbcon->query($qry);
	if ($result !== false) {
		//error_log("Entrando a listar, consulta válida");
		while ($infoCol = $result->fetch_field()) {
			if ($infoCol->type == 252 && $infoCol->flags & 128) {
				array_push($binarias, $infoCol->name);
				error_log("Encuentra la columna {$infoCol->name} como binaria");
			}
		}
		while($fila = $result->fetch_assoc()) {
			foreach($binarias as $bin) {
				$fila[$bin] = 'data:' . $fila[$bin."_mime"] . ';base64,'.base64_encode($fila[$bin]);
			}
			array_push($retval, $fila);
		}
	}
	return $retval;
}
function creaPaginador($tabla, $dbcon, $condicion = "") {
	$retval = '<table><tr>';
	if ($condicion != "") $condicion = " where " . $condicion;
	$qry = "select count(*) from vc_$tabla $condicion;";
	$result = $dbcon->query($qry) or die($qry);
	$cnt = $result->fetch_row();
	$total = $cnt[0];
	$pags = ceil($total / REGS_PAGINA);
	for ($p = 0; $p < $pags; $p++) {
		$retval .= '<td><a href="javascript:void(0);" onclick="busca(' . $p . ')">' . ($p + 1) . '</a></td>';
	}
	$retval .= '</tr></table>';
	return $retval;
}


?>
