<?php 
require_once('./defs.php');
require_once('./funciones.php');
require_once('conexion.php');//se implementara la 
$retval = array("error" => "99", "errmsg"=>"Indefinido");
if (isset($_POST['token'])) {
	header('Content-Type: text/html; charset=utf-8');
	if ($dbcon = conectaDB()) {
		if(validaToken($_POST['token'], $dbcon, $_POST['r'], $_POST['idU'])) {
			$id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
			switch ($_POST['t']) {
				case 1:		//Cancela la venta
					$qry = "update venta set estado = 'Cancelada' where id = '{$id}';";
					break;
				case 2:		//Da por surtida la venta
					$qry = "update venta set estado = 'Surtida' where id = '{$id}';";
					break;
			}
			if ($dbcon->query($qry)) {
				$retval["error"] = "0";
			}
		} else {
			$retval["error"] = "14";
			$retval["errmsg"] = "Token inválido";
		}
    } else {
        $retval["error"] = "12";
        $retval["errmsg"] = "Problemas de base de datos";
	}
}
echo json_encode($retval);
