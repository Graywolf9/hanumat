<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
require_once('./defs.php');
require_once('./funciones.php');
require_once('conexion.php');//se implementara la 
$retval = array("error" => "99", "errmsg"=>"Indefinido");
if (isset($_POST['token'])) {
    header('Content-Type: text/html; charset=utf-8');
    if ($dbcon = conectaDB()) {
        if(validaToken($_POST['token'], $dbcon, $_POST['r'], $_POST['idU'])) {
            //Levantamos la conexión...*/
			$qry = "show full columns in " . $_POST['t'] . ";";
			if ($rs = $dbcon->query($qry)) {
				$retval["error"] = "0";
				$retval["campos"] = array();
				$retval["auxs"] = array();
				$retval["enums"] = array();
				while ($fila = $rs->fetch_assoc()) {
					error_log("Jalando campo: {$fila['Field']} con comentario {$fila{'Comment'}}");
					if ($fila['Comment'] != "") {
						array_push($retval["campos"], $fila);
						$pos = 0;
						if (($pos = strpos($fila['Comment'], "|C ")) !== FALSE) {
							//Vamos por la tabla...
							error_log("Encuentra auxiliar de {$fila['Field']}");
							//array_push($retval["auxs"], $fila['Field'] => array());
							$retval["auxs"][$fila['Field']] = array();
							$opts = substr($fila['Comment'], $pos + 3);
							error_log("Variable de opciones: $opts");
							$opts = explode(',', $opts);
							if (count($opts) == 2) {
								$qryOpts = "select id, $opts[1] as nombre from $opts[0] order by $opts[1];";
							} else {
								$qryOpts = "select $opts[1] as id, $opts[2] as nombre from $opts[0] order by $opts[2];";
							}
							error_log("Consulta para las opciones: $qryOpts");
							if ($result = $dbcon->query($qryOpts)) {
								while ($row = $result->fetch_assoc()) {
									array_push($retval["auxs"][$fila['Field']], $row);
								}
								$result->free();
							} else {
								error_log("No pudo traer tabla externa con: $qryOpts");
							}
						}
						if (strpos($fila['Type'], "enum(") !== FALSE) {
							array_push($retval["enums"], array($fila["Field"] => array()));
							preg_match("/^enum\(\'(.*)\'\)$/", $fila['Type'], $matches);
							$enum = explode("','", $matches[1]);
							foreach ($enum as $value) {
								array_push($retval["enums"][$fila["Field"]], $enum);
							}
						}
					}
				}
			}
		} else {
			$retval["error"] = "14";
			$retval["errmsg"] = "Token inválido";
		}
    } else {
		$retval["error"] = "12";
		$retval["errmsg"] = "Base de datos";
	}
}
error_log("Respondiendo: " . print_r($retval, true));
echo json_encode($retval);
