<?php
/**********************************************************************************************
*    Hanumat. PHP framework for fast and secure web application development
*
*    This file is part of Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as
*    published by the Free Software Foundation, either version 3 of the
*    License, or (at your option) any later version.
*
*    Hanumat is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
************************ Español ********************************
*
*    Hanumat. Entorno de trabajo PHP para desarrollo rápido y seguro de aplicaciones
*
*    Este archivo es parte de Hanumat.
*    Copyright (C) 2016-2018  Horus S. Rico / CarpathiaLab
*
*    Hanumat es software libre: usted puede redistribuirlo y/o modificarlo
*    bajo los términos de la Licencia Pública General GNU Affero tal y como
*    ha sido publicada por la Free Software Foundation, tanto la versión 3
*    de la Licencia o cualquier otra posterior.
*
*    Hanumat es distribuido en la esperanza de que llegue a ser útil,
*    pero SIN NINGUNA GARANTÍA; incluso sin implicar la garantía de
*    MERCHANTABILITY o FITNESS FOR A PARTICULAR PURPOSE.  Por favor refiérase a la
*    Licencia Pública General GNU Affero para más detalles.
*
*    Usted tiene que haber recibido una copia de la Licencia Pública General GNU Affero
*    con éste programa. De no ser así, vea <https://www.gnu.org/licenses/>.
*
**************************************************************************************/
require_once('defs.php');
require_once('conexion.php');
require_once('funciones.php');
$retval = "";
if (isset($_POST['token'])) {
	header('Content-Type: text/html; charset=utf-8');
	if ($dbcon = conectaDB()) {
		if (validaToken($_POST['token'], $dbcon, $_POST['r'], $_POST['idU'])) {
			//Aquí empieza lo gordo...
			$tabla = esclarece($_POST['t']);
			$arrJSON = array("registros" => array(), "error" => "0");
			//select idalumno, concat(alumnos.Nombre, ' ', alumnos.Apellidos) from alumnos where concat_ws(' ', nombre, apellidos) like ('%usua%') collate utf8_general_ci;
			$listado = esclarece($_POST['l']);
			$listado = explode("|", $listado, 2);
			$qry = "select $listado[0] as value, $listado[1] as label from $tabla where " . resuelveCond($_POST['c']) ." and $listado[1] like('%{$_POST['te']}%') collate utf8_general_ci limit 50;";
			if ($rs = $dbcon->query($qry)) {
				while ($fila = $rs->fetch_assoc()) {
					array_push($arrJSON['registros'], $fila);
				}
			} else {
				$arrJSON['error'] = '15';
				$arrJSON['errmsg'] = "Consulta fallida: $qry";
			}
			$retval = json_encode($arrJSON);
		} else {
			$retval = '{"error":"14", "errmsg":"Token inválido"}';
		}
	} else {
		$retval = '{"error":"21", "errmsg":"No hay datos"}';
	}
} else {
	//Petición incorrecta
	$retval = '{"error":"11", "errmsg":"Sesión inválida"}';
}
echo($retval);
?>