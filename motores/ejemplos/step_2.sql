-- Creamos las tablas de la aplicación...
create table producto (
  id int auto_increment,
  nombre varchar(60) not null comment 'Nombre del producto:',
  precio decimal(12,2) not null comment 'Precio:',
  fecha_alta timestamp default current_timestamp,
  primary key(id),
  unique key kNom(nombre)
);
create table venta (
  id int auto_increment,
  idcliente int not null comment 'Cliente:|C cliente,id,nombre',
  momento timestamp default current_timestamp(),
  notas text comment 'Notas de venta',
  primary key(id)
);
create table venta_detalle (
  idventa int not null,
  cantidad int default '1' comment 'Cantidad:',
  id_producto int not null comment 'Producto:|C producto,id,nombre',
  precio decimal(12,2) comment 'Precio:',
  primary key(idventa, id_producto)
);
delimiter $$
drop trigger if exists venta_detalle_BEFORE_INSERT $$
CREATE TRIGGER `venta_detalle_BEFORE_INSERT` BEFORE INSERT ON `venta_detalle` FOR EACH ROW
BEGIN
declare ultID integer default -1;
if (new.idventa = '-1') then
select max(id) into ultID from venta;
set new.idventa = ultID;
end if;
END $$
delimiter ;
