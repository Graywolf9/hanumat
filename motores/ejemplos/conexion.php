<?php
	/*	Se recomienda tener un usuario en la DB por cada aplicación.
	*	El usuario así mismo debe de tener solamente los privilegios
	*	SELECT, INSERT, UPDATE, EXECUTE
	*	y el privilegio DELETE se establece SOLAMENTE para las tablas que vayan a poder eliminar registros.
*/
	function conectaDB() {
		$servidor = '__SERVER__';	//Servidor de base de datos, no está obligado que sea local
		$dbnom = '__DB__';	//Nombre de la base de datos
		$usuario = '__USER__';	//Usuario para la aplicación de la base de datos
		$password = '__PASS__';	//Contraseña
		$puerto = 3714;	//Puerto para el servidor...
		$conectID=new mysqli($servidor, $usuario, $password, $dbnom, $puerto);
		if(!$conectID->connect_error) {
			$conectID->set_charset('utf8');
			return $conectID;
		} else {
			return null;
		}
	}
?>
