<?php
session_start();
require_once("./motores/interno/funciones.php");
require_once("./motores/interno/validador.php");
require_once("./motores/interno/defs.php");
require_once("./motores/interno/MasterCat.class.php");
$TABLA = 'producto';
$catalogo = new Catalogo($TABLA, 'P', null);
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?= NOMBRE_APLICACION ?></title>
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<link rel="stylesheet" href="./css/jquery-ui.min.css" />
		<?= $catalogo->getScripts() ?>
		<script type="text/javascript">
			function dibuja(datos) {
				//console.log($("#frmAlta").serialize());
				$("#vista").html("");
				var contenido = "";
				var hdr = '<tr>';
				for (var i = 0; i < datos.length; i++) {
					contenido += '<tr>';
					for (var llave in datos[i]) {
						if (datos[i].hasOwnProperty(llave)) {
							if (i == 0) {
								//Ponemos los encabezados
								if (!(llave == "id" || llave.indexOf('mime') > -1)) hdr += '<th>' + llave + '</th>';
							}
							if (!(llave == "id" || llave.indexOf('mime') > -1)) {
								if (datos[i][llave] != null && datos[i][llave].substr(0, 10) == 'data:image') 
									contenido += '<td><img src="' + datos[i][llave] + '" style="max-height: 150px" /></td>';
								else
									contenido += '<td>' + datos[i][llave] + '</td>';
							}
						}
					}
					contenido += '<td><button onclick="<?= $TABLA ?>.edita(\'' + datos[i]["id"] + '\')">Editar</button> <button onclick="<?= $TABLA ?>.borra(\'' + datos[i]["id"] + '\')">Borrar</button></td></tr>';
				}
				contenido = '<table>' + hdr + '</tr>' + contenido + '</table>';
				$("#vista").html(contenido);
			}
			function errorDatos() {
				alert("Hubo problemas con los datos");
			}
			function inicia() {
				<?= $TABLA ?>.busca();
			}
			function agregaRegistro() {
				if (<?= $TABLA ?>.valida()) {
					$.ajax({
						url: "motores/hanumat.php",
						type: "POST",
						dataType: "JSON",
						data: $("#frmAlta").serialize()
					}).done(function (r) {
						if (r.error == '0') {
							$("#mensaje").html("<div class='alert alert-success'> Grabado correctamente </div>");
							setTimeout(function(){
								<?= $TABLA ?>.busca();
								$("#mensaje").html('');
							}, 1200);
						}else if (r.error == '2') {
							$("#mensaje").html("<div class='alert alert-danger'> Ocurrió un error </div>");
							setTimeout(function(){
								$("#mensaje").html('');
							}, 3000);
						}
						
						<?= $TABLA ?>.limpia();
					});
				}
			}
			function doLogout() {
				$.ajax({
					url : "motores/hanumat.php",
					type : "POST",
					dataType : "JSON",
					data : {
						r : 's'
					}
				}).done(function(r) {
					if (r.error == '0') {
						window.location.reload();
					}
				});
				return false;
			}
		</script>
	</head>
	<body onload="inicia()">
		<h1><?= NOMBRE_APLICACION ?> -- Productos</h1>
		<div id="formulario">
			<h3>Añadiendo clientes</h3>
			<form id="frmAlta">
				<table>
					<?= $catalogo->comoTabla() ?>
				</table>
				<input type="hidden" name="r" value="invalido" />
				<input type="hidden" name="ae" id="ae" value="" />
				<a href="#" onclick="agregaRegistro()">Guardar</a>
			</form>
			<div id="mensaje"></div>
		</div>
		<div id="listado">
			<table id="vista">
			</table>
		</div>
		<button onclick="doLogout()">Salir</button>
	</body>
</html>
