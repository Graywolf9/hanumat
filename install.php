<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

  if (file_exists("motores/ejemplos/ready.php")){
    header('Location: .');
  }

  // if (isset($_GET["req"])){
  if (isset($_POST["req"])){
    // header('Content-Type: application/json');

    // Preparar el archivo de conexion
    if (!file_exists("motores/interno/conexion.php")){
      // $conexion_file = shell_exec('cat motores/ejemplos/conexion.php');
      $cn_file = fopen("motores/ejemplos/conexion.php","r");
      $conexion_file = fread($cn_file,filesize("motores/ejemplos/conexion.php"));
      fclose($cn_file);
      $conexion_file = str_replace("__SERVER__",$_POST["server"],$conexion_file);
      $conexion_file = str_replace("__DB__",$_POST["base"],$conexion_file);
      $conexion_file = str_replace("__USER__",$_POST["usuario"],$conexion_file);
      $conexion_file = str_replace("__PASS__",$_POST["contra"],$conexion_file);

      $cn_file = fopen("motores/interno/conexion.php", "w");
      fwrite($cn_file,$conexion_file);
      fclose($cn_file);
    }

    require_once("motores/interno/conexion.php");
    if ($dbcon = conectaDB()){
      $mysql = "mysql";
      if ($_POST["os"]=="Macintosh"){
        $mysql = "/Applications/MAMP/Library/bin/mysql";
      }

      $comando = 'cat motores/ejemplos/step_1.sql | '.$mysql.' -u '.$_POST["usuario"].' -p'.$_POST["contra"].' '.$_POST["base"];
      shell_exec($comando);

      $query = "SELECT * FROM usuario WHERE email='".$_POST["correo"]."'";
      if ($resultado = $dbcon->query($query)){
        if ($resultado->num_rows <= 0){
          $query = "INSERT into usuario (email, passwd, nombrecompleto, idrol) VALUES ('".$_POST["correo"]."',password('".$_POST["u_contra"]."'),'".$_POST["nombre_u"]."',1);";
          mysqli_query($dbcon,$query) or die("Falló la multiconsulta: (" . $dbcon->errno . ") " . $dbcon->error);
        }
      }

      $comando = 'cat motores/ejemplos/step_2.sql | '.$mysql.' -u '.$_POST["usuario"].' -p'.$_POST["contra"].' '.$_POST["base"];
      shell_exec($comando);
    }

    if (!file_exists("motores/interno/defs.php")){
      $file = fopen("motores/ejemplos/defs.php","r");
      $file_content = fread($file,filesize("motores/ejemplos/defs.php"));
      fclose($file);
      $file_content = str_replace("__DOMINIO__",$_POST["server"],$file_content);
      $file_content = str_replace("__NOMBRE__",$_POST["nombre_app"],$file_content);

      $file = fopen("motores/interno/defs.php", "w");
      fwrite($file,$file_content);
      fclose($file);
    }

    $file = fopen("motores/ejemplos/ready.php", "w");
    fwrite($file,"Installed");
    fclose($file);

    header('Location: .');

    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" type="image/png" href="hanumat.png" />
  <title>Hanumat - Install</title>
  <link rel="stylesheet" href="css/install.css">
  <script src="js/jquery.min.js" charset="utf-8"></script>
</head>
<body>
  <div id="header">
    <h1>Bienvenido a Hanumat</h1>
  </div>
    <form id="form" class="" action="./install.php" method="post">
      <h3>Por favor ingrese los datos de su aplicación:</h3>
      <fieldset class="">
        <legend>Aplicación:</legend>
        Nombre de la aplicación: <input type="text" name="nombre_app" value=""><br/>
        Nombre usuario: <input type="text" name="nombre_u" value=""><br/>
        Correo: <input type="email" name="correo" value=""><br/>
        Contraseña: <input type="password" name="u_contra" value=""><br/>
      </fieldset>
      <fieldset class="">
        <legend>Base de datos:</legend>
        Servidor: <input type="text" name="server" value=""><br/>
        Base de datos: <input type="text" name="base" value=""><br/>
        Usuario: <input type="text" name="usuario" value=""><br/>
        Contraseña: <input type="password" name="contra" value=""><br/>
      </fieldset>
      <input type="hidden" name="req" value="">
      <input type="hidden" name="os" value="">
      <div class="button"><input id="continuar" type="submit" name="" value="Continuar"></div>
    </form>
</body>
<script type="text/javascript">
  document.querySelector("#form").addEventListener("submit",function(e){
    if (navigator.userAgent.indexOf("Macintosh") >= 0){
      document.querySelector("input[name=os]").value = "Macintosh";
    }
  });
  // document.querySelector("#continuar").addEventListener("click",function(e){
  //   e.preventDefault();
  //   var data = {};
  //
  //   formData = new FormData(document.querySelector("#form"));
  //   for (var [key, value] of formData.entries()) {
  //     if (value == ""){
  //       // alert("Necesitas llenar todos los campos");
  //       // return false;
  //     }
  //   }
  //   formData.forEach(function(value, key){
  //     data[key] = value;
  //   });
  //   data["req"] = "";
  //   $.post("./install.php",data,function(d){
  //     console.log(d);
  //   });
  // });
</script>
</html>
