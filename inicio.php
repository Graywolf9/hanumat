<?php
session_start();
require_once("./motores/interno/funciones.php");
require_once("./motores/interno/validador.php");
require_once("./motores/interno/defs.php");
require_once("./motores/interno/MasterCat.class.php");
$TABLA = "venta";
$DETALLE = "venta_detalle";
$venta = new Catalogo($TABLA, "P", null);
$productos = new Catalogo($DETALLE, "D", $venta->getConexion());
$venta->setNombreForma("frmVentas");
$productos->setNombreForma("frmVentas");
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?= NOMBRE_APLICACION ?></title>
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<?= $venta->getScripts() ?>
		<?= $productos->getScripts() ?>
		<script type="text/javascript">
			function doLogout() {
				$.ajax({
					url : "motores/hanumat.php",
					type : "POST",
					dataType : "JSON",
					data : {
						r : 's'
					}
				}).done(function(r) {
					if (r.error == '0') {
						window.location.reload();
					}
				});
				return false;
			}
			function editaDetalle(id) {
				<?= $DETALLE ?>.edita(id);
			}
			function dibujaDetalle(obj, tbl) {
				var fila = '<tr>';
				for (var prop in obj) {
					if (obj[prop] != '-1') {
						if (obj[prop].indexOf("cache||") != -1) {
							fila += '<td><img src="' + $("#vprevias img")[0].src + '" style="max-width: 300px; max-height: 300px;"></td>';
							$("#vprevias img")[0].src = "";
							document.getElementById("tmp" + prop).disabled = false;
						} else if (obj[prop].indexOf("data:image") != -1) {
							fila += '<td><img src="' + obj[prop] + '" style="max-width: 300px; max-height: 300px;"></td>';
						} else if (prop == '<?= ofusca('venta_detalle_id_producto') ?>') {
							var s = document.getElementById(prop);
							var ctrl = s.options[s.selectedIndex];
							fila += '<td>' + ctrl.text + '</td>';
						} else {
							fila += '<td>' + obj[prop] + '</td>';
						}
					}
				}
				fila += '<td><p onclick="quitaFila(this)"> - </p></td></tr>';
				$("#tbl"+tbl).append(fila);
			}
			function quitaFila(obj) {
				<?= $DETALLE ?>.quitaFila("tbl<?= $DETALLE ?>",obj);
			}
			function agregaRegistro() {
				if (<?= $TABLA ?>.valida()) {
					$.ajax({
						url: "motores/hanumat.php",
						type: "POST",
						dataType: "JSON",
						data: $("#frmVentas").serialize()
					}).done(function (r) {
						if (r.error == '0') {
							$("#mensaje").html("<div class='alert alert-success'> Grabado correctamente </div>");
							setTimeout(function(){
								<?= $TABLA ?>.busca();
								$("#mensaje").html('');
							}, 1200);
						}else if (r.error == '2') {
							$("#mensaje").html("<div class='alert alert-danger'> Ocurrió un error </div>");
							setTimeout(function(){
								$("#mensaje").html('');
							}, 3000);
						}
						<?= $TABLA ?>.limpia();
						<?= $DETALLE ?>.limpiaDetalle();
					});
				}
			}
			function dibuja(datos) {
				//console.log($("#frmAlta").serialize());
				$("#vista").html("");
				var contenido = "";
				var hdr = '<tr>';
				for (var i = 0; i < datos.length; i++) {
					contenido += '<tr>';
					for (var llave in datos[i]) {
						if (datos[i].hasOwnProperty(llave)) {
							if (i == 0) {
								//Ponemos los encabezados
								if (!(llave == "id" || llave.indexOf('mime') > -1)) hdr += '<th>' + llave + '</th>';
							}
							if (!(llave == "id" || llave.indexOf('mime') > -1)) {
								if (datos[i][llave] != null && datos[i][llave].substr(0, 10) == 'data:image') 
									contenido += '<td><img src="' + datos[i][llave] + '" style="max-height: 150px" /></td>';
								else
									contenido += '<td>' + datos[i][llave] + '</td>';
							}
						}
					}
					contenido += '<td><button onclick="cambiaVenta(\'' + datos[i]["id"] + '\', 1)">Cancelar venta</button> <button onclick="cambiaVenta(\'' + datos[i]["id"] + '\', 2)">Surtido</button></td></tr>';
				}
				contenido = '<table>' + hdr + '</tr>' + contenido + '</table>';
				$("#vista").html(contenido);
			}
			function errorDatos() {
				alert("Hubo problemas con los datos");
			}
			function inicia() {
				<?= $TABLA ?>.busca();
			}
			function cambiaVenta(id, modo) {
				var datos = {
					r: 'x', 
					f: '<?= ofusca('procesaventa.php') ?>', 
					id: id,
					t: modo
				};
				$.ajax({
					url : "motores/hanumat.php",
					type : "POST",
					dataType : "JSON",
					data : datos
				}).done((r) => {
					if (r.error == '0') {
						<?= $TABLA ?>.busca();
					} else {
						alert(r.errmsg);
					}
				});
			}
		</script>
	</head>
	<body onload="inicia()">
		<header>
			<h1>Felicidades, estás firmado en <?= NOMBRE_APLICACION ?></h1>
			<nav id="menu">
				<a href="./clientes.php">Catálogo de clientes</a>
				<a href="./productos.php">Catálogo de productos</a>
				<button onclick="doLogout()">Salir</button>
			</nav>
		</header>
		<section id="ventas">
			<h3>Registrar una venta</h3>
			<div>
				<form id="frmVentas">
					<input type="hidden" id="ae" name="ae" value="" />
					<input type="hidden" id="ae" name="r" value="dameID" />
					<table>
						<?= $venta->comoTabla() ?>
					</table>
					<h5>Productos a vender</h5>
					<?= $productos->comoTabla() ?>
				</form>
				<button onclick="agregaRegistro()">Guardar venta!</button>
			<div>
			<div id="mensaje">
			</div>
		</section>
		<section id="historial">
			<table id="vista">
			</table>
		</section>
	</body>
</html>
