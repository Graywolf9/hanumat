<?php
if (!file_exists("motores/ejemplos/ready.php")){
	header('Location: ./install.php');
}

require_once("./motores/interno/defs.php");
session_start();
//Las siguientes líneas deben estar presentes en pantallas de inicio de sesión.
$hayUsuario = (isset($_SESSION['Usuario']) && $_SESSION['Usuario']['rol'] != 't');
if (!$hayUsuario) {
	$_SESSION['Usuario'] = array("rol" => "t", "publica" => "invalida");
	?>
	<html>
		<head>
			<link rel="icon" type="image/png" href="hanumat.png" />
			<title><?= $NOMBRE_APLICACION ?></title>
			<script src="js/jquery.min.js"></script>
			<script src="js/jquery-ui.min.js"></script>
			<link rel="stylesheet" href="./css/jquery-ui.min.css" />
			<script type="text/javascript">
				function firmado(event){
					//alert(event.keyCode);
					if(event == undefined || event.keyCode == 13){
						$.ajax({
							url : './motores/hanumat.php',
							type : 'post',
							dataType : 'JSON',
							data : {
								r : 'l',
								usr : $("#usuario").val(),
								pwd : $("#pwd").val()
							}
						}).done(function(r) {
							if (r.error == '0') {
								window.location.href = "./" + r.pagina;
							} else {
								console.log(r);
								alert('Error de credenciales');
							}
						});
						return false;
					}
				}
			</script>
		</head>
		<body>
			<h1>Punto de venta Hanumat</h1>
			<div>
				<h2>Usuario y contraseña</h2>
				<input type="email" id="usuario" placeholder="Correo del usuario" onkeypress="firmado(event)" /></br>
				<input type="password" id="pwd" placeholder="Contraseña" onkeypress="firmado(event)" /></br>
				<a href="#" onclick="firmado()">Entrar</a>
			</div>
		</body>
	</html>
<?php
} else {
	//Esta sección redirecciona a la página de inicio de la aplicación cuando ya hay una sesión activa.
	header('Status: 301 Moved Permanently', false, 301);
	header('Location: ./' . $_SESSION['Usuario']['pag_inicial']);
}
?>
